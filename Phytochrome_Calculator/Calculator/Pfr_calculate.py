import os
from generate_inputs import *

from Calculator import *

class Dummy(object):
    def __init__(self, inputs):
        self.k = inputs[0]
        self.s = inputs[1]
        self.e = inputs[2]
        self.m = inputs[3]
        self.t = inputs[4]
        self.o = inputs[5]

if __name__=="__main__":
    inputs = generate_inputs()
    calculator(Dummy(inputs))
