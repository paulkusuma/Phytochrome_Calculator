import numpy as np

def unpack_file(input,option):

    if option == 0:

        f = open(input, 'r')
        lines = f.read().splitlines()[3:]
        temp = []
        for i in range(0, len(lines)):
            values = lines[i].split(",")
            row = []
            for j in range(0, len(values)):
                row.append(float(values[j]))
            temp.append(row)
        results = []
        for j in range(0,len(temp[0])):
            temp2 = []
            for i in range(0,len(temp)):
                temp2.append(temp[i][j])
            results.append(temp2)

    elif option == 1:

        f = open(input, 'r')
        lines = f.read().splitlines()[3:]
        temp = []
        for i in range(0, len(lines)):
            values = lines[i].split(",")
            row = []
            for j in range(0, len(values)):
                row.append(float(values[j]))
            temp.append(row)
        results = np.zeros((len(temp),len(temp[0])))
        for i in range(0,len(temp)):
            for j in range(0,len(temp[i])):
                results[i,j] = temp[i][j]

    elif option == 2:

        f = open(input,'r')
        lines = f.read().splitlines()[4:]
        temp1 = []
        for i in range(0,len(lines)):
            values = lines[i].split(",")
            row = []
            if len(values)%2 == 1:
                for j in range(0,len(values)-1):
                    row.append(float(values[j]))
                temp1.append(row)
            else:
                for j in range(0,len(values)):
                    row.append(float(values[j]))
                temp1.append(row)
        temp3 = []
        for i in range(0,len(temp1[0])/2):
            temp2 = []
            k = 0
            for j in range(0,len(temp1)):
                if len(temp1[k][2*i:2*(i+1)]) != 0:
                    temp2.append(temp1[k][2*i:2*(i+1)])
                    k += 1
                else:
                    break
            temp3.append(temp2)
        results = []
        for i in range(0,len(temp3)):
            temp4 = np.zeros((len(temp3[i]),2))
            for j in range(0,len(temp3[i])):
                temp4[j,:] = temp3[i][j]
            results.append(temp4)
    out = results

    return out