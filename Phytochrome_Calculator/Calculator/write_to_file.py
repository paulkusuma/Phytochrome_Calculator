import numpy as np
import datetime

def write_to_file(options,k,s,e,nms,irr,simulations):

    file = open(options.o, "w")

    file.write('# ')
    file.write(datetime.datetime.now().strftime('%d-%m-%Y %H:%M'))
    file.write('\n')

    file.write('# Files:\n')

    file.write('# Experimental conditions: ')
    file.write(options.e)
    file.write('\n')

    file.write('# Photoconversion spectra: ')
    file.write(options.s)
    file.write('\n')

    file.write('# System parameters: ')
    file.write(options.k)
    file.write('\n')
    
    file.write('# Model simulated: ')
    file.write(options.m)
    file.write('\n')
    
    file.write('# Simuation time: ')
    file.write(options.t)
    file.write('\n')

    file.write('# If numeric simulation then output is\n[Pr-Pr, Pr-Pfr, Pfr-Pfr, Pr (total), Pfr (total), Total Pfr in nucleus, Total Pfr-Pfr in nucleus, Total phy in speckles]\n')
    file.write('# If analytic simulation then output is\n[Pr-Pr, Pr-Pfr, Pfr-Pfr, Pr (total), Pfr (total)]\n')
    file.write('\n')

    count = 0
    for ks in range(1,len(k)+1):
        file.write('# Parameter set: %d \n' % ks)

        for ss in range(1,1+(len(s[0,:])-1)/2):
            file.write('\t # Photoconversion Spectra: %d \n' % ss)

            for es in range(0,len(e)):
                file.write('\t\t # Experimental conditions: \n')
                if len(e[es]) > 1:
                    min_nm = nms[es][0]
                    max_nm = nms[es][-1]
                    total_irr = sum(irr[es])
                    file.write('\t\t\t # Wavelengths (nm): %.2f ' % min_nm + '--- %.2f \n' % max_nm)
                    file.write('\t\t\t # Total irradiance (umol.m^(-2).s^(-1)): %.2f \n' % total_irr)
                else:
                    file.write('\t\t\t # Wavelength (nm): %.2f \n' % nms[es])
                    file.write('\t\t\t # Irradiance (umol.m^(-2).s^(-1)): %.2f \n' % irr[es])
                file.write('\t\t\t\t')
                np.savetxt(file, simulations[count], fmt='%.2f', newline=' ')
                file.write('\n\n')
                count+=1

    file.close()

    return simulations