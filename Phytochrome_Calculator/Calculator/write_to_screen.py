import numpy as np
import datetime

def write_to_screen(options,k,s,e,nms,irr,simulations):

    print('# ' + datetime.datetime.now().strftime('%d-%m-%Y %H:%M') + '\n')

    print('# Files:\n')

    print('# Experimental conditions: ' + options.e + '\n')

    print('# Photoconversion spectra: ' + options.s + '\n')

    print('# System parameters: ' + options.k + '\n')
    
    print('# Model simulated: ' + options.m + '\n')
    
    print('# Simuation time: ' + options.t + '\n')

    print('# If numeric simulation the output is\n[Pr-Pr, Pr-Pfr, Pfr-Pfr, Pr (total), Pfr (total), Total Pfr in nucleus, Total Pfr-Pfr in nucleus, Total phy in speckles]\n')
    print('# If analytic simulation the output is\n[Pr-Pr, Pr-Pfr, Pfr-Pfr, Pr (total), Pfr (total)]\n\n')

    count = 0
    for ks in range(1,len(k)+1):
        print('# Parameter set: %d \n' % ks)

        for ss in range(1,1+(len(s[0,:])-1)/2):
            print('\t # Photoconversion Spectra: %d \n' % ss)

            for es in range(0,len(e)):
                print('\t\t # Experimental conditions: \n')
                if len(e[es]) > 1:
                    min_nm = nms[es][0]
                    max_nm = nms[es][-1]
                    total_irr = sum(irr[es])
                    print('\t\t\t # Wavelengths (nm): %.2f ' % min_nm + '--- %.2f \n' % max_nm)
                    print('\t\t\t # Total irradiance (umol.m^(-2).s^(-1)): %.2f \n' % total_irr)
                else:
                    print('\t\t\t # Wavelength (nm): %.2f \n' % nms[es])
                    print('\t\t\t # Irradiance (umol.m^(-2).s^(-1)): %.2f \n' % irr[es])
                print('\t\t\t')
                print(['%.2f' % sims for sims in simulations[count]])
                print('\n\n')
                count+=1

    return simulations