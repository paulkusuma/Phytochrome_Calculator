### 07-02-17: Rob Smith (robert1.smith@wur.nl / r.w.smith.ncl09@googlemail.com) ###

Explanation of formatting for input files.

In this folder you will find four example input files - one each for the system parameters and the photoconversion spectra, and two for the experimental conditions.

———————

‘system_parameters.txt’

In this file is a column vector of the parameters used to simulate the system, see Table 1 of the accompanying manuscript. The order of the parameters is important and must match the order given in the vector above the parameter values, i.e. [k_dr,k_dfr,k_r1,k_r2,k_31,k_32,k_41,k_42,k_5,k_in,M,a,b].

In principle, one could extend this table to include multiple columns (i.e. if the use wished to simulate the system with multiple parameter sets) and to do this, commas need to separate the values. For example, if a user wanted to compare simulations where dark reversion is present and absent, then 0’s would need to be used for dark reversion parameters in one simulation.

6.099999999999999737e-04,6.099999999999999737e-04
4.957000000000000028e+01,4.957000000000000028e+01
4.969999999999999973e-01,0
5.100000000000000366e-03,0
1.360000000000000098e+00,1.360000000000000098e+00
4.429999999999999716e+00,4.429999999999999716e+00
4.099999999999999645e+00,4.099999999999999645e+00
1.300000000000000044e-01,1.300000000000000044e-01
2.999999999999999889e-02,2.999999999999999889e-02
5.839999999999999858e+01,5.839999999999999858e+01
6.000000000000000000e+00,6.000000000000000000e+00
5.600000000000000533e-01,5.600000000000000533e-01
9.000000000000000000e+00,9.000000000000000000e+00

———————

‘photoconversion_default.txt’

This file contains the photoconversion rates published by A. Mancinelli (1994) for plant phytochromes, see Table 2 of the accompanying manuscript. The first column contains the wavelength (in nm) whilst columns 2 and 3 contain the photoconversion rates (in m2/mol) for Pr to Pfr transformations (sig_r) and Pfr to Pr transformations (sig_fr). If one has evidence for the use of different photoconversion rates (e.g. if the user wished to simulate the phyB(1-650) N-terminal fragment) then the file needs to be formatted in the same way with the sig_r and sig_fr columns replaced.

Furthermore, a user can perform the calculations for multiple photoconversion spectra by adding extra columns to the input file. e.g.

# [nm,sig_r1,sig_fr1,sig_r2,sig_fr2]
300.0,1404.0,728.3,312.1,559.7
310.0,906.1,572.7,241.6,409.0
320.0,677.7,481.0,188.7,280.7
330.0,609.0,366.9,146.5,198.6
340.0,723.7,314.1,114.7,146.1
350.0,981.2,328.0,94.08,113.4
… etc …

———————

Experimental conditions files: in principle these are generated automatically by the script by answering questions, however users can also provide the input files in one of two formats, or a combination of the two.

‘experimental_conditions.txt’

This file shows how the input needs to be formatted if the user wants to simulate lab conditions, i.e. the user wants to know the levels of each species under specific wavelengths and intensities. In this situation a list is generated with pairs of wavelength (in nm) and intensity (in umol/m2s) appended to one another.

NOTE: that at the end of the list a comma needs to be placed - this is a Python issue when interpreting input lists.

‘experimental_conditions1.txt’

This file shows how the input needs to be formatted if the user wants to know the phytochrome species when a plant is exposed to an entire light spectrum (for example if grown under natural sunlight). In this situation columns needs to be created with wavelength (in nm, for accuracy of the results we suggest that this column increases by 1 nm in each row) in the first column and intensity (in umol/m2s) in the second column.

NOTE: in this situation, commas do not need to appear at the end of each row.

In principle, users can also mix these different types of inputs by creating and input file with

# [nm,N,nm,N,nm,N]
178.0,8.22808E-06,665.0,5.0,740.0,5.0
179.0,-0.000683872
180.0,7.657E-05
181.0,-1.84419E-06
182.0,6.05263E-06
183.0,2.59459E-06
… etc …

where three experimental conditions are simulated - the first is a light spectrum, the second is a single wavelength (665 nm) and the third is a second single wavelength (740 nm).