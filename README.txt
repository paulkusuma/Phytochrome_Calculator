### 07-02-17: Rob Smith (robert1.smith@wur.nl / r.w.smith.ncl09@googlemail.com) ###

Phytochrome Pfr Calculator as detailed in Smith & Fleck (2017) (in 'Phytochromes: Methods and Protocols', ed. A. Hiltbrunner, Springer)

In the following folders contains:

Phytochrome_Calculator/Calculator - the Python scripts required to calculate percentages of phyB species relative to total phyB in the system.
Phytochrome_Calculator/Input_files - default input files for the calculator to function.

The phyB system is defined by the model of C. Klose et al. (2015) [doi: 10.1038/NPLANTS.2015.90].

Pre-requisites required to run the script:
Python version 2.7 (https://www.python.org/downloads/)
NumPy version 1.8 (https://sourceforge.net/projects/numpy/files/NumPy/)
SciPy version 0.13 (https://sourceforge.net/projects/scipy/files/scipy/)

To run the script, type the following into the command line terminal from within the 'Phytochrome_Calculator' folder:
python2.7 ./Calculator/Pfr_calculate.py

You are then asked a range of questions, whereby a user can enter specific values of the paths to specific input files containing the required information.
Note that all new input files must match the format of the default and example file provided in the folder Input_files/
Please see the README.txt file in Input_files/ for further information.

——————————————————

1. Would you like to change any system parameters?

If yes ('Y' or 'y') you are asked to enter:
a. the filename of the text file containing the system parameters (for an example see Input_files/system_parameters.txt) or
b. specific parameter names (e.g. k_r1) and a given value. The input file is then automatically generated.

Please note that the lists of parameter names and values should NOT end in a comma (e.g. “k_r1,k_r2,”).

If no ('N' or 'n') then calculations shall be conducted using the default published parameter set (Input_files/system_parameters.txt).

——————————————————

2. Would you like to use the default photoconversion spectra?

If yes ('Y' or 'y') then the photoconversion spectra defined by A. Mancinelli are used in the simulations (Input_files/photoconversion_default.txt).

If no ('N' or 'n') then you are requested to enter the filename of the text file containing new transition rates.

——————————————————

3. Do you have an input file of measured light intensities?

If yes ('Y' or 'y') then you are requested to enter the filename of the text file containing vectors of measured light spectra.

If no ('N' or 'n') then you are asked to define for which wavelengths you would like to measure phyB species (e.g. 665 [nm]) and give the value of the light intensity (e.g. 1 [umol/m2s]). An input file is automatically generated given your inputs.

Please note that the lists of wavelength and intensities should NOT end in a comma (e.g. “665,666,676,”) and should not be too long. If you wish to compute phytochrome species from an entire light spectrum or from a large range of experimental conditions it is more efficient to create an input file.

If no input file is selected then a second question is asked about the experimental conditions…

3a. Do you want to know species amounts at each wavelength or integrated over all entered wavelengths?

The user then has an option as to whether they wish to calculate the levels of different species at each individual wavelength or integrate over the entire spectrum. This would be useful if, for example, a user wanted to know how phytochrome pools changed under different sunlight conditions (in which case the light a plant is exposed to comes from the entire light spectrum) or if the user wants to know how phytochrome pools change in the lab (where single wavelengths are used).

Enter ‘1’ if you want to know the phytochrome amounts at each specific wavelength, or ‘2’ if you want to know the total amount of species from a single light spectrum.

——————————————————

4. Would you like to solve the system analytically or numerically?

If analytically ('A' or 'a') then the calculation of relative phyB species depends solely on the transition rates and dark reversion rates. Here the full system has been simplified and solved analytically.

If numerically ('N' or 'n') then the full system is simulated the relative phyB amounts are calculated. Given that the system can be simulated for any time-period you are also asked to enter how long you want the simulation to take place for (in minutes).

Note: under high light intensity the resulting values of both the analytical and numerical solutions should match. However, under low light intensities, where dark reversion and degradation reactions play a more prominent role, there is likely to be some differences in the solutions.

——————————————————

5. Please enter path for results file:

Here you can enter the filename of an output text file where the results will be printed. All files are automatically saved in Phytochrome_Calculator/Output_files. This output is also printed to the screen.